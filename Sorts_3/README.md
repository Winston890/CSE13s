Winston Yi
wyi10
Spring 2021
#Assignment 3: Getting Your Affairs in Order

-----------------------
DESCRIPTION
In this assignment I have created four types of sorting algorithms. Quicksort (stack and queue), bubble sort, and shell sort. 
I have also made a test harness for these four sorts. The primary purpose of implementing these sorts is to understand time 
complexity with a focus on worst case (Big O).

-----------------------
FILES

-
set.c/h

This file contains the program code for the sets.

-
bubble.c/h

This file contains the code for the bubble sort.

-
sorting.c

This file contains a test harness to test my sorts.

-
compare.c/h

This file contains a code to keep track of my moves and compares.

-
gaps.h

This file contains the gap sequence for shell.c/h

-
queue.c/h

This file contains code for the quicksort queue.

-
quick.c/h

This file contains code for my quicksort.

-
shell.c/h

This file contains code for my shell sort.

-
stack.c/h

This file contains code for my quicksort stack.
-
Makefile

This file contains instructions needed to compile the code

-
DESIGN.pdf

This file contains the process of my thinking and explanations of my designs.

-
README.md

This file contains information about the instructions and files needed to run the library.

-----------------------
INSTRUCTIONS

Type make in the terminal to create an executable called "sorting". Run the file with "./sorting" and follow the instructions printed out.
