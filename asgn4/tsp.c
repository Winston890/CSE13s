#include "graph.h"
#include "path.h"
#include "stack.h"
#include "vertices.h"

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#define OPTIONS "hvui:o:"

// Discard came from eric
static uint32_t discard;
static int count = 0;
bool verbose = false;

// Heavily debugged by Eric Hernandez god give up a raise he helps so much get him that $$$
// Also debugged by Miles, both kings who need a treasury give em more hours
void dfs(Graph *G, Path *p, Path *p2, uint32_t v, char *places[], FILE *file_pointer_out) {
    count += 1;
    graph_mark_visited(G, v);
    path_push_vertex(p, v, G);
    if ((path_length(p) >= path_length(p2)) && (path_length(p2) != 0)) {
        return;
    }
    // if there is hamiltonian path
    // p2 is shortest path found so far
    if ((path_vertices(p) == graph_vertices(G)) && graph_has_edge(G, v, START_VERTEX)) {
        path_push_vertex(p, START_VERTEX, G);
        // Copy path to shortest path is current path < shortest path
        if (path_length(p) < path_length(p2) || path_length(p2) == 0) {
            path_copy(p2, p);
            if (verbose == true) {
                fprintf(file_pointer_out, "Path_length: %" PRIu32 "\n", path_length(p2));
                path_print(p2, file_pointer_out, places);
            }
        }
        path_pop_vertex(p, &discard, G);
    }
    for (uint32_t w = 0; w < graph_vertices(G); w++) {
        if (graph_has_edge(G, v, w) && !(graph_visited(G, w))) {
            dfs(G, p, p2, w, places, file_pointer_out);
        }
    }
    path_pop_vertex(p, &discard, G);
    graph_mark_unvisited(G, v);
}

typedef enum graph_types { h, v, u, i, o } graph_types;
// Structure inspired by the asgn3.pdf
int main(int argc, char **argv) {
    int opt = 0;
    bool help = false;
    bool undirected = false;
    FILE *file_pointer_in = stdin;
    FILE *file_pointer_out = stdout;
    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        // Flag each program depending on what the program argument was
        case 'h': help = true; break;
        case 'v': verbose = true; break;
        case 'u': undirected = true; break;
        case 'i':
            if ((file_pointer_in = fopen(optarg, "r")) == NULL) {
                printf("error opening files");
                fclose(file_pointer_out);
                exit(1);
            }
            break;
        case 'o':
            if ((file_pointer_out = fopen(optarg, "w")) == NULL) {
                printf("error opening filesdf");
                fclose(file_pointer_in);
                exit(1);
            }
            break;
        default: help = true;
        }
    }

    if (help == true) {
        fprintf(file_pointer_out,
            "SYNOPSIS\n  Traveling Salesman Problem using DFS.\n\nUSAGE\n  ./tsp [-u] [-v] [-h] "
            "[-i "
            "infile] [-o outfile]\n\nOPTIONS\n  -u             Use undirected graph.\n  -v         "
            "    Enable verbose printing.\n  -h             Program usage and help.\n  -i infile   "
            "   Input containing graph (default: stdin)\n  -o outfile     Output of computed path "
            "(default: stdout)\n");
        fclose(file_pointer_in);
        fclose(file_pointer_out);
        return 0;
    }

    char file_buffer[512];
    if (fgets(file_buffer, 512, file_pointer_in) == NULL) {
        printf("error opening file");
        exit(1);
    }

    int n = atoi(file_buffer);
    if (n > VERTICES) {
        printf("Too many sadge");
        exit(1);
    }

    char *places[n];
    for (int i = 0; i < n; i++) {
        fgets(file_buffer, 512, file_pointer_in);
        int len = strlen(file_buffer);
        if (file_buffer[len - 1] == '\n') {
            file_buffer[len - 1] = '\0';
        }
        places[i] = strdup(file_buffer);
    }

    Graph *G = graph_create(n, undirected);
    Path *p = path_create();
    Path *p2 = path_create();
    int i, j, k, c;
    while ((c = (fscanf(file_pointer_in, "%d %d %d\n", &i, &j, &k))) != EOF) {
        if (c != 3) {
            break;
        }
        graph_add_edge(G, i, j, k);
    }
    dfs(G, p, p2, START_VERTEX, places, file_pointer_out);
    fprintf(file_pointer_out, "Path length: %d\n", path_length(p2));
    fprintf(file_pointer_out, "Path: ");
    path_print(p2, file_pointer_out, places);
    fprintf(file_pointer_out, "Total recursive calls: %d\n", count);
    fclose(file_pointer_in);
    fclose(file_pointer_out);
    graph_delete(&G);
    path_delete(&p);
    path_delete(&p2);
        free(*places);
}
