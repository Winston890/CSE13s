#include "graph.h"

#include "stack.h"
#include "vertices.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct Graph {
    uint32_t vertices;
    bool undirected;
    bool visited[VERTICES];
    uint32_t matrix[VERTICES][VERTICES];
};

Graph *graph_create(uint32_t vertices, bool undirected) {
    Graph *G = (Graph *) malloc(sizeof(Graph));
    G->vertices = vertices;
    G->undirected = undirected;
    for (int i = 0; i < VERTICES; i++) {
        G->visited[i] = false;
    }
    for (int i = 0; i < VERTICES; i++) {
        for (int j = 0; j < VERTICES; j++) {
            G->matrix[i][j] = 0;
        }
    }
    assert(G);
    return G;
}

void graph_delete(Graph **G) {
    free(*G);
    *G = NULL;
}

bool graph_add_edge(Graph *G, u_int32_t i, uint32_t j, uint32_t k) {
    assert(G);
    G->matrix[i][j] = k;
    if (G->undirected == true) {
        G->matrix[j][i] = k;
    }
    return true;
}

uint32_t graph_vertices(Graph *G) {
    return G->vertices;
}

bool graph_has_edge(Graph *G, uint32_t i, uint32_t j) {
    if (G->matrix[i][j] != 0) {
        return true;
    }
    return false;
}

uint32_t graph_edge_weight(Graph *G, uint32_t i, uint32_t j) {
    return G->matrix[i][j];
}

bool graph_visited(Graph *G, uint32_t v) {
    if (G->visited[v] == false) {
        return false;
    }
    return true;
}

void graph_mark_visited(Graph *G, uint32_t v) {
    G->visited[v] = true;
}

void graph_mark_unvisited(Graph *G, uint32_t v) {
    G->visited[v] = false;
}

void graph_print(Graph *G) {
    for (int i = 0; i < VERTICES; i++) { // columns, so for columns 1:
        for (int j = 0; j < VERTICES; j++) { // rows, iterate through entire row to print first line
            printf("%d ", G->matrix[i][j]);
        }
        printf("\n");
    }
}
