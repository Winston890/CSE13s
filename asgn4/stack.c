#include "stack.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Credits to Eugene and Sahiti Vallamreddy
struct Stack {
    uint32_t top;
    uint32_t capacity;
    uint32_t *items;
};

Stack *stack_create(uint32_t capacity) {
    Stack *s = (Stack *) malloc(sizeof(Stack));
    s->top = 0;
    s->capacity = capacity;

    s->items = (uint32_t *) malloc(capacity * sizeof(uint32_t));
    if (s->items == NULL) {
        free(s);
        return NULL;
    }
    assert(s);
    return s;
}

void stack_delete(Stack **s) {
    free((*s)->items);
    free(*s);
    *s = NULL;
}

bool stack_push(Stack *s, uint32_t x) {
    assert(s);
    if (s->top == s->capacity) {
        return false;
    }
    s->items[s->top] = x;
    s->top += 1;
    return true;
}

uint32_t stack_size(Stack *s) {
    return s->top;
}

//Developed together with Eric
void stack_copy(Stack *dst, Stack *src) {
    if (dst != NULL && src != NULL) {
        uint32_t len = 0;
        if (src->top < dst->capacity) {
            len = src->top;
        } else {
            len = dst->capacity;
        }
        for (uint32_t i = 0; i < len; i++) {
            dst->items[i] = src->items[i];
        }
        dst->top = len;
    }
}

bool stack_empty(Stack *s) {
    return (s->top == 0);
}

bool stack_pop(Stack *s, uint32_t *x) {
    if (s->top == 0) {
        return false;
    }
    s->top -= 1;
    *x = s->items[s->top];
    return true;
}

bool stack_peek(Stack *s, uint32_t *x) {
    if (s->top == 0) {
        return false;
    }
    *x = s->items[s->top - 1];
    return true;
}

// Credits to Eugene Chou for stackprint
void stack_print(Stack *s, FILE *outfile, char *cities[]) {
    for (uint32_t i = 0; i < s->top; i++) {
        fprintf(outfile, "%s", cities[s->items[i]]);
        if (i + 1 != s->top) {
            fprintf(outfile, " -> ");
        }
    }
    fprintf(outfile, "\n");
}

bool stack_full(Stack *s) {
    return s->top == s->capacity;
}
