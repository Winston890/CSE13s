Winston Yi
wyi10
Spring 2021
#Assignment 4: The Circumnavigations of Denver Long

-----------------------
DESCRIPTION
In this assignment I have created a program that will find the shortest hamiltonian path given up to 26 vertices. The program can read and write to files if enabled in the command line options.

-----------------------
FILES

-
vertices.h

This file contains two definitions for vertices

-
tsp.c

This file contains the Depth-first search function and the code needed to read and write to files.

-
graph.c/h

This file contains the code needed to create the Graph ADT.

-
path.c/h

This file contains the code needed to create the Path ADT.

-
stack.c/h

This file contains code to create the stack for my Path ADT.

-
Makefile

This file contains instructions needed to compile the code

-
DESIGN.pdf

This file contains the process of my thinking and explanations of my designs.

-
README.md

This file contains information about the instructions and files needed to run the program.

-----------------------
INSTRUCTIONS

Ensure that all files are present. Type make in the terminal to create an executable called "tsp". To understand the various command line options, type "./tsp -h ". 
An example command will show with the proper formatting and short explanations of each command line option.
"-h" will print the help message, "-i [file]" will specify the file to take as the input with stdin as default. 
The command "-o [file]" will specify the file to write to as the output with stdout as the default, "-u" will interpret the input as an undirected graph, and "-v" will print out every hamiltonian path the program comes across.
