#include "path.h"

#include "graph.h"
#include "stack.h"
#include "vertices.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

struct Path {
    Stack *vertices;
    uint32_t length;
};

Path *path_create(void) {
    Path *p = (Path *) malloc(sizeof(Path));
    p->vertices = stack_create(VERTICES);
    p->length = 0;
    assert(p);
    return p;
}

void path_delete(Path **p) {
    stack_delete(&(*p)->vertices);
    free(*p);
    *p = NULL;
}

bool path_push_vertex(Path *p, uint32_t v, Graph *G) {
    uint32_t previous_vertice;
    if (stack_empty(p->vertices)) {
        p->length = graph_edge_weight(G, START_VERTEX, v);
        return stack_push(p->vertices, v);
    }
    stack_peek(p->vertices, &previous_vertice);
    if (stack_push(p->vertices, v) == true) {
        p->length += graph_edge_weight(G, previous_vertice, v);
        return true;
    }
    return false;
}

bool path_pop_vertex(Path *p, uint32_t *v, Graph *G) {
    uint32_t previous_vertice = 0;
    if (stack_pop(p->vertices, v)) {
        stack_peek(p->vertices, &previous_vertice);
        p->length -= graph_edge_weight(G, previous_vertice, *v);
        return true;
    }
    return false;
}

uint32_t path_vertices(Path *p) {
    return stack_size(p->vertices);
}

uint32_t path_length(Path *p) {
    return p->length;
}

void path_copy(Path *dst, Path *src) {
    stack_copy(dst->vertices, src->vertices);
    dst->length = src->length;
}

void path_print(Path *p, FILE *outfile, char *cities[]) {
    stack_print(p->vertices, outfile, cities);
}
