Winston Yi
wyi10
Spring 2021
#Assignment 1: LRC

-----------------------
DESCRIPTION
In this assignment I have created a program to run a game of left, right, center where virtual players gamble.

-----------------------
FILES

-
lrc.c

This file contains the program code

-
Makefile

This file contains instructions needed to compile the code

-
DESIGN.pdf

This file contains the process of my thinking and explanations of my designs.

-
philos.h

This file contains an array of the philosopher names.

-
README.md

This file contains information about the instructions and files needed to run lrc.c

-----------------------
INSTRUCTIONS

Type make in the terminal to create an executable called lrc. Run the file by typing "./lrc" and input the seed and number of players.

