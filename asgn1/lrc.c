#include "philos.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Function to get the index of the winner
int get_winner(int *array, int length) {
    for (int i = 0; i < length; i++) {
        if (array[i] != 0) {
            return i;
        }
    }
    fprintf(stderr, "oopsie woopsie sumthing went vvery wwwong UwU");
    exit(1);
}

int main() {
    int players, pot, seed, players_in, bank, int_checker;
    typedef enum faciem { PASS, LEFT, RIGHT, CENTER } faces;
    faces die[] = { LEFT, RIGHT, CENTER, PASS, PASS, PASS };

    // Set the seed
    printf("Random seed: ");
    scanf("%d", &seed);
    int_checker = seed;

    //Check for valid input
    if (seed != int_checker || seed < 1) {
        printf("Enter a valid integer\n");
        exit(1);
    }
    srandom(seed);

    // Create an array of size num players with their balances all 3
    printf("How many players? ");
    scanf("%d", &players);

    //Check for valid input
    int_checker = players;
    if (players != int_checker || players < 1 || players > 14) {
        printf("Enter a valid number of players\n");
        exit(1);
    }

    // Set the array to all 3
    int array[players];
    for (int i = 0; i <= (players - 1); i++) {
        array[i] = 3;
    }

    players_in = players;
    pot = 0;

    // Only 1 player
    if (players == 1) {
        printf("%s won $3. Also has no friends to play with. Go make some friends instead of "
               "gambling.\n",
            philosophers[0]);
        exit(1);
    }

    while (players_in > 1) {
        // Iterate through the players
        for (int i = 0; i < players; i++) {
            if (players_in == 1) {
                bank = array[i];
                break;
            }
            // If they're out, skip the player
            if (array[i] == 0) {
                continue;
            }
            printf("%s rolls...", philosophers[i]);

            // Rolls == amount of money (max 3 rolls)
            int num_rolls = array[i];
            if (num_rolls > 3) {
                num_rolls = 3;
            }

            for (int j = 0; j < num_rolls; j++) {
                //Roll the die with random() and proceed accordingly
                int roll = random() % 6;
                if (die[roll] == LEFT || die[roll] == RIGHT) { // Give money to players
                    int pos = 0;
                    if (die[roll] == LEFT) {
                        pos = ((i + players - 1)
                               % players); // Credits to Darrell Long for this line of code
                    } else if (die[roll] == RIGHT) {
                        pos = ((i + 1) % players);
                    }
                    if (array[pos] == 0) {
                        players_in += 1;
                    }
                    array[pos] += 1;
                    array[i] -= 1;
                    printf(" gives $1 to %s", philosophers[pos]);
                } else if (die[roll] == CENTER) { // put money in pot
                    printf(" puts $1 in the pot");
                    pot += 1;
                    array[i] -= 1;

                } else if (die[roll] == PASS) { // do nothing
                    printf(" gets a pass");
                }
            }
            // Decrement players in if they're out
            if (array[i] == 0) {
                players_in -= 1;
            }
            printf("\n");
        }
    }
    int winner = get_winner(array, players); // Get index of winner
    printf("%s wins the $%d pot with $%d left in the bank!\n", philosophers[winner], pot, bank);
    return 0;
}
