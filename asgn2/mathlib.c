#include "mathlib.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>

#define EPSILON 1e-10

// Done in Tristan's section
// Returns Absolute Value
static inline double Abs(double x) {
    return x < 0 ? -x : x;
}

// Credits to Prof Long for the sqrt
double Sqrt(double x) {
    double y = 1.0;
    assert(x >= 0);
    for (double guess = 0.0; Abs(y - guess) > EPSILON; y = (y + x / y) / 2.0) {
        guess = y;
    }
    return y;
}

// Credits to Prof Long for the exp
double Exp(double x) {
    double term = 1, sum = 1;
    for (int k = 1; Abs(term) > EPSILON; k += 1) {
        term *= x / k;
        sum += term;
    }
    return sum;
}

// Code was inspired by Tristan's, Miles', and Winson's section
// Accuracy approaching one (done with the if statements) inspired by Miles' section
double arcSin(double x) {
    // If you're approaching -1, then return the expression in terms of cos which is centered closer to 0
    if (Abs(-1 - x) < EPSILON) {
        return -arcCos(Sqrt(1 - x * x));
    }
    if (Abs(1 - x) < EPSILON) {
        return arcCos(Sqrt(1 - x * x));
    }
    double sum = x;
    double top = x;
    double bottom = 1.0;
    double coefficient = 1.0;
    // Code for the pattern in the Mclaurin series for Sin
    // Starts at 3 because the first term is x, which is already given by our definitions
    // of our variables
    for (double i = 3.0; Abs(coefficient * top / bottom) > EPSILON; i += 2.0) {
        top *= x * x;
        coefficient *= (i - 2) / (i - 1);
        bottom = i;
        sum += coefficient * top / bottom;
    }
    return sum;
}

double arcCos(double x) {
    // Since arccos = pi/2 - arcsin
    return M_PI / 2 - arcSin(x);
}

double arcTan(double x) {
    // Another trig identity for tan
    return arcSin(x / Sqrt(x * x + 1));
}

// Returns the x intercept of the tangent line at x
// Repeats that process until a suitable approximation has been found
double Log(double x) {
    double next = 1.0;
    double previous = 0.0;
    while (Abs(next - previous) > EPSILON) {
        previous = next;
        next = previous - ((Exp(previous) - x) / Exp(previous));
    }
    return next;
}
