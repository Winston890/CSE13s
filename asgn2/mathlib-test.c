#include "mathlib.h"

#include <math.h>
#include <stdio.h>
#include <unistd.h>

#define OPTIONS "asctl"

// Structure inspired by the asgn2.pdf
int main(int argc, char **argv) {
    int opt = 0;

    // Flags for each function
    int arcsin = 0;
    int arccos = 0;
    int arctan = 0;
    int log_flag = 0;

    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {

        // Flag each program depending on what the program argument was
        case 'a':
            arcsin = 1;
            arccos = 1;
            arctan = 1;
            log_flag = 1;
            break;
        case 's': arcsin = 1; break;
        case 'c': arccos = 1; break;
        case 't': arctan = 1; break;
        case 'l': log_flag = 1; break;
        default:
            // In case it somehow hits default error handling
            printf("Program usage: ./mathlib-test -[asctl]\n");
            printf("  -a   Runs all tests (arcsin, arccos, arctan, log)\n");
            printf("  -s   Runs arcsin tests\n");
            printf("  -c   Runs arccos tests\n");
            printf("  -t   Runs arctan tests\n");
            printf("  -l   Runs log tests\n");
        }
    }

    // If no arguments are given
    if (argc == 1) {
        printf("Program usage: ./mathlib-test -[asctl]\n");
        printf("  -a   Runs all tests (arcsin, arccos, arctan, log)\n");
        printf("  -s   Runs arcsin tests\n");
        printf("  -c   Runs arccos tests\n");
        printf("  -t   Runs arctan tests\n");
        printf("  -l   Runs log tests\n");
    }

    // Printing each table if they were flagged
    if (arcsin == 1) {
        printf("  x            arcSin           Library        Difference\n");
        printf("  -            ------           -------        ----------\n");
        for (double x = -1.0; x < 1.0; x += 0.1) {
            double approx = arcSin(x);
            double lib = asin(x);
            double dif = arcSin(x) - asin(x);
            printf("%7.4lf%16.8lf%16.8lf%16.10lf\n", x, approx, lib, dif);
        }
    }

    if (arccos == 1) {
        printf("  x            arcCos           Library        Difference\n");
        printf("  -            ------           -------        ----------\n");
        for (double x = -1.0; x < 1.0; x += 0.1) {
            printf("%7.4lf%16.8lf%16.8lf%16.10lf\n", x, arcCos(x), acos(x), arcCos(x) - acos(x));
        }
    }

    if (arctan == 1) {
        printf("  x            arcTan           Library        Difference\n");
        printf("  -            ------           -------        ----------\n");
        for (double x = 1; x < 10; x += 0.1) {
            printf("%7.4lf%16.8lf%16.8lf%16.10lf\n", x, arcTan(x), atan(x), arcTan(x) - atan(x));
        }
    }

    if (log_flag == 1) {
        printf("  x            Log              Library        Difference\n");
        printf("  -            ------           -------        ----------\n");
        for (double x = 1; x < 10; x += 0.1) {
            printf("%7.4lf%16.8lf%16.8lf%16.10lf\n", x, Log(x), log(x), Log(x) - log(x));
        }
    }
}
